import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.blueGrey[900],
        appBar: AppBar(
          backgroundColor: Colors.blueGrey[900],
          centerTitle: true,
          title: const Text(
            'Calculadora',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        body: const Calculadora(),
      ),
    );
  }
}

class Calculadora extends StatefulWidget {
  const Calculadora({super.key});

  @override
  State<Calculadora> createState() => _CalculadoraState();
}

class _CalculadoraState extends State<Calculadora> {
  String tela = '';

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Card(
          color: Colors.blueGrey[800],
          margin: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 2.0),
          child: Container(
            padding: const EdgeInsets.all(10.0),
            height: 150.0,
            width: MediaQuery.of(context).size.width * 1,
            alignment: Alignment.centerRight,
            child: Text(
              tela,
              style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 50.0),
            ),
          ),
        ),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = 'limpar tela';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    'CE',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
            const Spacer(),
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = 'Apagar';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '<=',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
            const Spacer(),
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = 'Porcentagem';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '%',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
            const Spacer(),
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = 'Divisão';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '/',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = '7';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '7',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
            const Spacer(),
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = '8';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '8',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
            const Spacer(),
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = '9';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '9',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
            const Spacer(),
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = 'Multiplicação';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '*',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = '4';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '4',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
            const Spacer(),
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = '5';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '5',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
            const Spacer(),
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = '6';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '6',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
            const Spacer(),
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = 'Subtração';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '-',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = '1';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '1',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
            const Spacer(),
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = '2';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '2',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
            const Spacer(),
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = '3';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '3',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
            const Spacer(),
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = 'Soma';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '+',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 183.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = '0';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '0',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
            const Spacer(),
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = ',';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    ',',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
            const Spacer(),
            Container(
              margin: const EdgeInsets.all(2.0),
              height: 80.0,
              width: 80.0,
              decoration: BoxDecoration(
                color: Colors.blueGrey[800],
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black,
                    spreadRadius: 1.0,
                    blurRadius: 4.0,
                    offset: Offset(0, 0),
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(25.0),
                ),
              ),
              child: ElevatedButton(
                onPressed: () {
                  tela = '=';
                  setState(() {
                    tela;
                  });
                },
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blueGrey[800]),
                  overlayColor: MaterialStateProperty.all(Colors.blueGrey[600]),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
                child: const Center(
                  child: Text(
                    '=',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 25.0,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
